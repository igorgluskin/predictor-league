// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAlH_D3fBTN3o7TJUvqZ0ueJEBZ4iNZncY',
    authDomain: 'predictor-league-5138e.firebaseapp.com',
    databaseURL: 'https://predictor-league-5138e-default-rtdb.firebaseio.com',
    projectId: 'predictor-league-5138e',
    storageBucket: 'predictor-league-5138e.appspot.com',
    messagingSenderId: '706889377444',
    appId: '1:706889377444:web:18497e7836c669c291a831',
    measurementId: 'G-PNJHM4HDZH'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries
//
// // Your web app's Firebase configuration
// // For Firebase JS SDK v7.20.0 and later, measurementId is optional
// const firebaseConfig = {
//   apiKey: "AIzaSyAlH_D3fBTN3o7TJUvqZ0ueJEBZ4iNZncY",
//   authDomain: "predictor-league-5138e.firebaseapp.com",
//   databaseURL: "https://predictor-league-5138e-default-rtdb.firebaseio.com",
//   projectId: "predictor-league-5138e",
//   storageBucket: "predictor-league-5138e.appspot.com",
//   messagingSenderId: "706889377444",
//   appId: "1:706889377444:web:18497e7836c669c291a831",
//   measurementId: "G-PNJHM4HDZH"
// };
//
// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
