import { Component, ViewChild } from '@angular/core';
import { IonModal, ModalController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';
import { NewLeagueComponent } from '../components/new-league/new-league.component';
import { NewLeagueModule } from '../components/new-league/new-league.module';
import { Router } from '@angular/router';


@Component({
  selector: 'pl-home-page',
  templateUrl: 'home-page.html',
  styleUrls: ['home-page.scss']
})
export class HomePage {

  public leagues = [
    {value: 1, name: 'Apples'},
    {value: 2, name: 'Oranges'},
    {value: 3, name: 'Bananas'},
  ];
  public modelData: any;
  public selectedLeague;
  message: string;

  constructor(public modalCtrl: ModalController, private router: Router) {}

  editProfile() {
    this.router.navigate(['profile']);
  }

  leagueSelected(league) {
    console.log('selected', league);
    if (league === '') {
      this.openLeagueModal();
      this.selectedLeague = '';
    } else {
      console.log(league);
    }
  }

  async openLeagueModal() {
    console.log('here', this.selectedLeague);
    const modal = await this.modalCtrl.create({
      component: NewLeagueComponent,
      backdropDismiss: true,
    });

    modal.onDidDismiss().then(data => {
      console.log('data: ', data.data);
      if  (data?.data) {
        console.log('not null');
      } else {
        console.log('null', this.leagues[0]);
        this.selectedLeague = this.leagues[0].value;
        console.log(this.selectedLeague);
      }
      console.log('closed', data);
    });

    await modal.present();
  }
}
