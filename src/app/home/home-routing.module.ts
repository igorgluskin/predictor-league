import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home-page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'matches',
        loadChildren: () => import('../components/matches/matches.module').then(m => m.MatchesModule)
      },
      {
        path: 'matches',
        loadChildren: () => import('../components/matches/matches.module').then(m => m.MatchesModule)
      },
      {
        path: 'table',
        loadChildren: () => import('../components/table/table.module').then(m => m.TableModule)
      },
      {
        path: '',
        redirectTo: '/home/matches',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/matches',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class HomePageRoutingModule {}
