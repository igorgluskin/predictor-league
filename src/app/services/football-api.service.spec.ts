/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FootballApiService } from './football-api.service';

describe('Service: FootballApi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FootballApiService]
    });
  });

  it('should ...', inject([FootballApiService], (service: FootballApiService) => {
    expect(service).toBeTruthy();
  }));
});
