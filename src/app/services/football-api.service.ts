import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FootballApiService {

constructor(private http: HttpClient) { }

public getFixtures(): Observable<any> {
  return this.http.get('https://api-football-v1.p.rapidapi.com/v3/fixtures', {
    headers: {
      'x-rapidapi-key': 'bbe7d82295mshe43b98a7f53d1fep1b30b1jsn0ca21495a17e',
      'x-rapidapi-host': 'api-football-v1.p.rapidapi.com'
    },
    params: {
      league: '39',
      season: '2020'
    }
  });
}

public getLeagues(): Observable<any> {
  return this.http.get<{response: any}>('https://api-football-v1.p.rapidapi.com/v3/leagues', {
    headers: {
      'x-rapidapi-key': 'bbe7d82295mshe43b98a7f53d1fep1b30b1jsn0ca21495a17e',
      'x-rapidapi-host': 'api-football-v1.p.rapidapi.com'
    }
  }).pipe(map(result => {
    console.log(result);
    return result.response;
  }));
}

}
