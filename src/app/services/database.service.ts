import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  public fixtures: AngularFireList<Fixture>;
  public predictions: AngularFireList<Prediction>;
  public standings: AngularFireList<Standing>;

  private dbPathFixtures = '/fixtures';
  private dbPathPredictions = '/predictions';
  private dbPathStandings = '/standings';

  constructor(private db: AngularFireDatabase, private auth: AuthService) {
    this.fixtures = db.list(this.dbPathFixtures);
    this.predictions = this.db.list(this.dbPathPredictions);
    this.standings = db.list(this.dbPathStandings);
  }

  getAllFixtures(): Observable<Fixture[]> {
    return this.fixtures.valueChanges();
  }

  updateFixture(key: string, value: any): any {
    this.fixtures.update(key, value);
  }

  getAllPredictions(): Observable<Prediction[]> {
    return this.predictions.valueChanges();
  }

  getUserPredictions(): Observable<Prediction[]> {
    return this.predictions.valueChanges().pipe(map((predictions: any) =>
      predictions.map((pred: any) => pred[this.auth.getUser().id]).filter((p: any) => !!p)
    ));
  }

  getPredictionsByFixture(fixtureId: string): Observable<Prediction[]> {
    return this.db.list(`${this.dbPathPredictions}/${fixtureId}`).valueChanges() as Observable<Prediction[]>;
  }

  updatePrediction(pred: Prediction) {
    return this.predictions.update(`${pred.fixtureId}/${pred.userId}`, pred);
  }

  getStandings() {
    return this.standings.valueChanges();
  }
}


export interface Fixture {
  id: string;
  date: string;
  time: string;
  status: string;
  teams: {
    home: Team;
    away: Team;
  }
  goals: {
    home: number;
    away: number;
  }
}

export interface Team {
  id: number;
  name: string;
  logo: string;
  winner: boolean;
}

export interface Standing {
  userId: string;
  userName: string;
  pointsTotal: number;
}

export class Prediction {
  constructor(fId: string, uId: string, uName: string) {
    this.home = 0;
    this.away = 0;
    this.userId = uId;
    this.fixtureId = fId;
    this.points = 0;
    this.userName = uName;
    this.isFinal = false;
  }

  home: number;
  away: number;
  userId: string;
  userName: string;
  fixtureId: string;
  points: number;
  isFinal: boolean;
  penaltyWinner?: string;
}
