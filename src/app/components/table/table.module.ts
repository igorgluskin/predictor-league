import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { TableComponent } from './table.component';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, IonicSelectableModule],
  declarations: [ TableComponent ],
  exports: [ TableComponent ]
})
export class TableModule {}
