import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { MatchesComponent } from './matches.component';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, IonicSelectableModule],
  declarations: [ MatchesComponent ],
  exports: [ MatchesComponent ]
})
export class MatchesModule {}
