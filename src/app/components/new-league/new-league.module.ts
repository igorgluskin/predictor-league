import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { NewLeagueComponent } from './new-league.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, IonicSelectableModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, ReactiveFormsModule],
  declarations: [ NewLeagueComponent ],
  exports: [ NewLeagueComponent ]
})
export class NewLeagueModule {}
