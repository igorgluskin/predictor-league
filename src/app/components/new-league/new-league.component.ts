import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'pl-new-league',
  templateUrl: './new-league.component.html',
  styleUrls: ['./new-league.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {class: 'pl-new-league'}
})
export class NewLeagueComponent implements OnInit {

  public newLeague = false;
  public league: FormGroup;
  public playableLeague = null;
  public leagues: {name: string; id: string}[] = [
    {name: 'League One', id: '1'},
    {name: 'League Two', id: '2'},
    {name: 'League Three', id: '3'}
  ];

  constructor(
    private modalController: ModalController,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.league = this.fb.group({
      name: ['', Validators.required],
      passwordProtected: false,
      password: ''
    });

    this.league.get('passwordProtected').valueChanges.subscribe(val => {
      if (val) {
        this.league.get('password').setValidators(Validators.required);
      } else {
        this.league.get('password').clearValidators();
      }
      this.league.get('password').updateValueAndValidity();
    });
  }

  async joinLeague() {
    await this.modalController.dismiss(this.playableLeague);
  }

  async createLeague() {
    await this.modalController.dismiss(this.playableLeague);
  }

  async cancel() {
    await this.modalController.dismiss();
  }

}
