export interface League {
  name?: string;
  id?: string;
  passwordProtected?: boolean;
  password?: string;
  competition?: string;
  startDate?: Date;
  endDate?: Date;
  allowJoinInProgress?: boolean;
  scoring?: {
    outcome?: number;
    goalDiff?: number;
    winner?: number;
    reverseGoalDiff?: number;
    goals?: number;
  };
  predictionDeadline?: number;
}
