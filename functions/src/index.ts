/* eslint-disable max-len */
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
// import * as request from "request";

admin.initializeApp(functions.config().firebase);

// const db = admin.database();
exports.scheduledFunctionCrontab = functions.pubsub.schedule("1,21,41 * * * *")
    .timeZone("America/New_York")
    .onRun((context) => {
      console.log("This will run every 20 minutes");
      return null;
    });
//
// export const intervalUpdates = functions.pubsub.schedule("1,16,31,46 * * * *").onRun(() => {
//   console.log("interval");
// });

export interface Fixture {
  id: string;
  date: string;
  time: string;
  status: string;
  teams: {
    home: Team;
    away: Team;
  }
  goals: {
    home: number;
    away: number;
  }
}

export interface Team {
  id: number;
  name: string;
  logo: string;
  winner: boolean;
}

export interface Standing {
  userId: string;
  userName: string;
  pointsTotal: number;
}

export interface Prediction {
  home: number;
  away: number;
  userId: string;
  userName: string;
  fixtureId: string;
  points: number;
  isFinal: boolean;
}
